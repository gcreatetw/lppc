package com.gcreate.lppc;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gcreate.lppc.View.Activity_welcomePage;
import com.gcreate.lppc.WebAPI.LppcAPI;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.gcreate.lppc.WebAPI.LppcAPI.SERVER_IP;

public class global {

    public static LppcAPI lppcAPI;
    public static int windowWidth;
    public static int windowHeigh;
    // firebase
    public static boolean isappopen = false;


    public static void getWindowSize(Activity mActivity) {
        DisplayMetrics dm = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        global.windowHeigh = dm.heightPixels;
        global.windowWidth = dm.widthPixels;
    }




    public static void initlppcAPI() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {

                        Request.Builder builder = chain.request().newBuilder();
                        Request build = builder.build();
                        return chain.proceed(build);

                    }
                })
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_IP)
                .client(client) //Only for Web API debug
                .build();
        global.lppcAPI = retrofit.create(LppcAPI.class);

    }

    // Some function
    public static void replaceFragment(@Nullable Activity mActivity, Fragment fragment,String title) {
        FragmentManager fragmentManager = ((AppCompatActivity) mActivity).getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        fragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(fragment.toString());
        mActivity.getFragmentManager().popBackStack();
        fragmentTransaction.commit();
    }


    public static void setHideWindowStatusBar(Window window) {
        View decorView = window.getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(option);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

    }


    public static void sendNotification(Context context, String messageTitle, String messageBody) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "default_notification_channel_id";
        String channelDescription = "Others";
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = notificationManager.getNotificationChannel(channelId);
            if (notificationChannel == null) {
                int importance = NotificationManager.IMPORTANCE_HIGH; //Set the importance level
                notificationChannel = new NotificationChannel(channelId, channelDescription, importance);
                notificationChannel.setLightColor(Color.GREEN); //Set if it is necesssary
                notificationChannel.enableVibration(true); //Set if it is necesssary
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        Intent intent = new Intent(context, Activity_welcomePage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String[] split_line = messageBody.split("，");
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder;
        if (global.isappopen) {
            // in app catch notify
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.icon_notification_ippc)
                    .setColor(context.getResources().getColor(R.color.red_E60012))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),R.drawable.icon_notification_ippc))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(channelId);

        } else {
            notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.icon_notification_ippc)
                    .setColor(context.getResources().getColor(R.color.red_E60012))
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.icon_notification_ippc))
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setChannelId(channelId)
                    .setContentIntent(pendingIntent);
        }


        if (split_line.length != 0) {
            notificationBuilder.setContentTitle(messageTitle);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }
}
