package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.core.text.HtmlCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.lppc.Adapter.Adapter_authorContain;
import com.gcreate.lppc.Adapter.Adapter_category;
import com.gcreate.lppc.Adapter.CenterLayoutManager;
import com.gcreate.lppc.R;
import com.gcreate.lppc.WebAPI.ApiCallback;
import com.gcreate.lppc.WebAPI.ApiObjectBookWriterCats;
import com.gcreate.lppc.WebAPI.ApiObjectBookWriterContain;
import com.gcreate.lppc.databinding.FragmentArticleBookwriterBinding;
import com.gcreate.lppc.global;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Fragment_article_bookWriter extends Fragment implements ApiCallback {

    public static int catsSelectedPosition = 0;
    private static ApiObjectBookWriterCats objectBookWriterCats;
    private FragmentArticleBookwriterBinding binding;
    private ApiCallback mCallback;
    private List<String> catsList = new ArrayList<>();

    public Fragment_article_bookWriter() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article_bookwriter, container, false);
        mCallback = this;

        initView();
        getAuthorCats();

        return binding.getRoot();
    }

    private void initView() {
        binding.bookWriterComponentToolbar.componentToolbarTitle.setText(getArguments().getString("title"));
        binding.bookWriterComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.bookWriterComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        binding.bookWriterComponentImage.componentBannerImage.setBackgroundResource(R.mipmap.banner_book_writer);

    }


    private void getAuthorCats() {

        Call<ApiObjectBookWriterCats> call = global.lppcAPI.getAllBookWriterCats();
        call.enqueue(new Callback<ApiObjectBookWriterCats>() {

            @Override
            public void onResponse(Call<ApiObjectBookWriterCats> call, Response<ApiObjectBookWriterCats> response) {

                objectBookWriterCats = response.body();

                for (int i = 0; i < objectBookWriterCats.getCategorise().size(); i++) {
                    String itemName = objectBookWriterCats.getCategorise().get(i).getCategory_name();
                    catsList.add(i, HtmlCompat.fromHtml(itemName, HtmlCompat.FROM_HTML_MODE_LEGACY).toString());
                }
                mCallback.loadComplete();
                initAuthorCatsRecyclerView();
            }

            @Override
            public void onFailure(Call<ApiObjectBookWriterCats> call, Throwable t) {

            }
        });
    }

    private void initAuthorCatsRecyclerView() {
        RecyclerView authorCats = binding.bookWriterComponentRecyclerview.componentRecyclerViewCats;

        CenterLayoutManager centerLayoutManager = new CenterLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        Adapter_category adapterArticleCats = new Adapter_category(catsList);
        authorCats.setAdapter(adapterArticleCats);
        adapterArticleCats.setOnItemClickListener(new Adapter_category.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                catsSelectedPosition = position;
                centerLayoutManager.smoothScrollToPosition(authorCats, new RecyclerView.State(), position);
                getAuthors(position);
                adapterArticleCats.notifyDataSetChanged();
            }
        });
        authorCats.setLayoutManager(centerLayoutManager);
    }

    private void getAuthors(int position) {
        RecyclerView authorContain = binding.bookWriterComponentRecyclerview.componentRecyclerView;

        Call<ApiObjectBookWriterContain> call = global.lppcAPI.getBookWriterContain(objectBookWriterCats.getCategorise().get(position).getSlug());
        call.enqueue(new Callback<ApiObjectBookWriterContain>() {
            @Override
            public void onResponse(Call<ApiObjectBookWriterContain> call, Response<ApiObjectBookWriterContain> response) {

                ApiObjectBookWriterContain apiObjectBookWriterContain = response.body();

                authorContain.setLayoutManager(new GridLayoutManager(getContext(), 3));
//                authorContain.addItemDecoration(new GridSpacingItemDecoration(3, 0, true));
                Adapter_authorContain adapterAuthorContain = new Adapter_authorContain(apiObjectBookWriterContain);
                adapterAuthorContain.setOnItemClickListener(new Adapter_authorContain.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), Activity_webView.class);
                        intent.putExtra("LinkURL", apiObjectBookWriterContain.getRelated_authors().get(position).getAuthor_url());
                        startActivity(intent);
                    }
                });
                authorContain.setAdapter(adapterAuthorContain);

            }

            @Override
            public void onFailure(Call<ApiObjectBookWriterContain> call, Throwable t) {

            }
        });

    }

    @Override
    public void loadComplete() {
        getAuthors(0);
    }


}