package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.gcreate.lppc.MainActivity;
import com.gcreate.lppc.R;
import com.gcreate.lppc.global;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class Activity_welcomePage extends AppCompatActivity {

    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_page);

        global.getWindowSize(this);
        global.initlppcAPI();
        global.setHideWindowStatusBar(getWindow());

        // firebase function
        global.isappopen = true;
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        if( task.getResult() == null)
                            return;
                        // Get new Instance ID token
                        token = task.getResult().getToken();
                        // Log and toast
                        Log.i("Activity","token : "+token);
                    }
                });

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                Activity_welcomePage.this.finish();
            }
        }, 1000);

    }

}