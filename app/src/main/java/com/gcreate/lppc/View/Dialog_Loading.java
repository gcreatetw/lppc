package com.gcreate.lppc.View;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.gcreate.lppc.Listener.WebLoadingListenerManager;
import com.gcreate.lppc.R;
import com.gcreate.lppc.databinding.DialogLoadingBinding;
import com.gcreate.lppc.global;


public class Dialog_Loading extends DialogFragment {

    public Dialog_Loading() {
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getDialog() != null) {
            getDialog().getWindow().setLayout((int) (global.windowWidth), (int) (global.windowHeigh));
            getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        DialogLoadingBinding binding = DataBindingUtil.inflate(inflater, R.layout.dialog_loading, container, false);

        //  Loading 過程中. User 手動按下物理返回鍵， 防呆處理。
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    WebLoadingListenerManager.getInstance().sendBroadCast();
                    dialog.cancel();
                    return true;
                }
                return false;
            }
        });

        return binding.getRoot();
    }
}
