package com.gcreate.lppc.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.PermissionRequest;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.gcreate.lppc.Listener.WebLoadingListener;
import com.gcreate.lppc.Listener.WebLoadingListenerManager;
import com.gcreate.lppc.R;
import com.gcreate.lppc.databinding.ActivityWebViewBinding;

public class Activity_webView extends AppCompatActivity implements WebLoadingListener {

    private String url;
    private SharedPreferences sp;
    private Dialog_Loading loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityWebViewBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view);
        WebLoadingListenerManager.getInstance().registerListener(this);
        // Loading Animate
        loadingDialog = new Dialog_Loading();
        loadingDialog.show(this.getSupportFragmentManager(), "loading dialog");

        //獲得值
        Intent intent = getIntent();
        //傳值
        if (intent.getStringExtra("LinkURL") != null) {
            url = intent.getStringExtra("LinkURL");
        }
        binding.webviewComponentToolbar.componentToolbarTitle.setText("張老師文化");
        binding.webviewComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.webviewComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        // 開啟網頁設定
        //支持javascript
        binding.webview.getSettings().setJavaScriptEnabled(true);
        binding.webview.getSettings().setDomStorageEnabled(true);
        // 設置可以支持缩放
        binding.webview.getSettings().setSupportZoom(true);

        binding.webview.loadUrl(url);

        /* 站存 cookie .維持登入狀態*/
        binding.webview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // WebView不加载该Url
                view.loadUrl(url);
                //  获取cookies
                CookieManager cm = CookieManager.getInstance();
                String cookies = cm.getCookie(url);
                if (sp != null) {
                    sp.edit().putString("cook", cookies).apply();
                }
                CookieSyncManager.createInstance(getApplicationContext());
                CookieSyncManager.getInstance().sync();
                return true;
            }
        });
    }

    @Override
    public void notifyViewCancelCall() {
        loadingDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        WebLoadingListenerManager.getInstance().unRegisterListener(this);
    }
}
