package com.gcreate.lppc.View;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gcreate.lppc.Adapter.Adapter_homeCats;
import com.gcreate.lppc.Adapter.Adapter_homeCats_2;
import com.gcreate.lppc.Adapter.GridSpacingItemDecoration;
import com.gcreate.lppc.ItemDatas.ItemLists;
import com.gcreate.lppc.R;
import com.gcreate.lppc.databinding.FragmentHomeBindingImpl;
import com.gcreate.lppc.global;

public class Fragment_home extends Fragment {

    private FragmentHomeBindingImpl binding;
    public static final String LINE_PACKAGE_NAME = "jp.naver.line.android";

    public Fragment_home() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);

        initView();

        return binding.getRoot();

    }

    private void initView() {
        binding.homeToolbar.setTitle("");
        binding.homeToolbar.setNavigationIcon(null);


        try {
            ((AppCompatActivity) getActivity()).setSupportActionBar(binding.homeToolbar);
        } catch (Exception e) {
            Log.e("APPDebug", e.toString());
        }


        setHasOptionsMenu(true);
        bindRecycleViewData();

    }


    private void bindRecycleViewData() {

        binding.rvHomeCats.setLayoutManager(new GridLayoutManager(getContext(), 2));
        Adapter_homeCats adapterHomeCats = new Adapter_homeCats(ItemLists.list1());
        binding.rvHomeCats.addItemDecoration(new GridSpacingItemDecoration(2, (int) (global.windowWidth * 0.078), true));
        adapterHomeCats.setOnItemClickListener(new Adapter_homeCats.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (position == 0) {
                    // 寫書人
                    global.replaceFragment(getActivity(), new Fragment_article_bookWriter(),"寫書人");
                } else if (position == 1) {
                    // 出書人
                    global.replaceFragment(getActivity(), new Fragment_article_publisher(),"出書人");
                } else if (position == 2) {
                    // 說書人
                    Intent intent = new Intent(getActivity(), Activity_webView.class);
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/%e8%aa%aa%e6%9b%b8%e4%ba%ba/");
                    startActivity(intent);
                } else if (position == 3) {
                    // 最新消息
                    global.replaceFragment(getActivity(), new Fragment_article_latestNews(),"最新消息");
                }
            }
        });
        binding.rvHomeCats.setAdapter(adapterHomeCats);


        binding.rvHomeCats2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        Adapter_homeCats_2 adapterHomeCats2 = new Adapter_homeCats_2(ItemLists.list2());
        adapterHomeCats2.setOnItemClickListener(new Adapter_homeCats_2.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), Activity_webView.class);
                if (position == 0) {
                    // 雲商店
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/cloud-store/");
                } else if (position == 1) {
                    // 雲讀書會
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/%e9%9b%b2%e8%ae%80%e6%9b%b8%e6%9c%83/");
                } else if (position == 2) {
                    // 雲學苑
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/yun-xueyuan/all-yunxueyuan/");
                } else if (position == 3) {
                    // 訂閱誌
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/%e8%a8%82%e9%96%b1%e8%aa%8c/");
                }
                startActivity(intent);
            }
        });
        binding.rvHomeCats2.setAdapter(adapterHomeCats2);

    }

    //------------------------------------------Menu function  -----------------------------------------
    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_item, menu);
    }

    // 隱藏 Menu item 用
    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.toolbarMenuItem_fb) {
            Intent intent = new Intent(getActivity(), Activity_webView.class);
            intent.putExtra("LinkURL", "https://www.facebook.com/lppc.com.tw");
            startActivity(intent);

        } else if (item.getItemId() == R.id.toolbarMenuItem_line) {

            if (isAppInstalled(getActivity(), LINE_PACKAGE_NAME)) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("line://ti/p/%40468nouct"));
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "please install Line app", Toast.LENGTH_LONG).show();
            }

        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * check the app is installed
     */
    private boolean isAppInstalled(Context context, String packagename) {
        PackageInfo packageInfo;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packagename, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            e.printStackTrace();
        }
        if (packageInfo == null) {
            //System.out.println("没有安装");
            return false;
        } else {
            //System.out.println("已经安装");
            return true;
        }
    }
}