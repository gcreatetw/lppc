package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.lppc.Adapter.Adapter_authorContain;
import com.gcreate.lppc.Adapter.Adapter_publisherContain;
import com.gcreate.lppc.R;
import com.gcreate.lppc.WebAPI.ApiObjectBookWriterContain;
import com.gcreate.lppc.WebAPI.ApiObjectPublisherContain;
import com.gcreate.lppc.databinding.FragmentArticleLatestNewsBinding;
import com.gcreate.lppc.global;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 最新消息
 */
public class Fragment_article_latestNews extends Fragment {

    private FragmentArticleLatestNewsBinding binding;

    public Fragment_article_latestNews() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article_latest_news, container, false);

        initView();
        getLatestNewsContain();

        return binding.getRoot();
    }


    private void initView() {
        binding.latestNewsComponentToolbar.componentToolbarTitle.setText(getArguments().getString("title"));
        binding.latestNewsComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.latestNewsComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        binding.latestNewsComponentImage.componentBannerImage.setBackgroundResource(R.mipmap.banner_news);

    }

    private void getLatestNewsContain(){
        RecyclerView latestNewsContain = binding.latestNewsComponentRecyclerview.componentRecyclerView;

        Call<ApiObjectPublisherContain> call = global.lppcAPI.getLatestNews();
        call.enqueue(new Callback<ApiObjectPublisherContain>() {
            @Override
            public void onResponse(Call<ApiObjectPublisherContain> call, Response<ApiObjectPublisherContain> response) {

                ApiObjectPublisherContain objectPublisherContain = response.body();

                latestNewsContain.setLayoutManager(new GridLayoutManager(getContext(), 2));
                Adapter_publisherContain adapterAuthorContain = new Adapter_publisherContain(objectPublisherContain);
                adapterAuthorContain.setOnItemClickListener(new Adapter_publisherContain.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(),Activity_webView.class);
                        intent.putExtra("LinkURL",objectPublisherContain.getPosts().get(position).getPost_url());
                        startActivity(intent);
                    }
                });
                latestNewsContain.setAdapter(adapterAuthorContain);

            }

            @Override
            public void onFailure(Call<ApiObjectPublisherContain> call, Throwable t) {

            }
        });

    }

}