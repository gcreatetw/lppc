package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.lppc.Adapter.Adapter_category;
import com.gcreate.lppc.Adapter.Adapter_publisherContain;
import com.gcreate.lppc.Adapter.CenterLayoutManager;
import com.gcreate.lppc.R;
import com.gcreate.lppc.WebAPI.ApiCallback;
import com.gcreate.lppc.WebAPI.ApiObjectPublisherCats;
import com.gcreate.lppc.WebAPI.ApiObjectPublisherContain;
import com.gcreate.lppc.databinding.FragmentArticlePublisherBinding;
import com.gcreate.lppc.global;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * 出書人
 */
public class Fragment_article_publisher extends Fragment implements ApiCallback {

    public static int catsSelectedPosition = 0;
    private static ApiObjectPublisherCats objectPublisherCats;
    private FragmentArticlePublisherBinding binding;
    private ApiCallback mCallback;
    private final List<String> catsList = new ArrayList<>();

    public Fragment_article_publisher() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article_publisher, container, false);
        mCallback = this;

        initView();
        getPublisherCats();

        return binding.getRoot();
    }

    private void initView() {
        binding.publisherComponentToolbar.componentToolbarTitle.setText(getArguments().getString("title"));
        binding.publisherComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.publisherComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        binding.publisherComponentImage.componentBannerImage.setBackgroundResource(R.mipmap.banner_publisher);

    }

    private void getPublisherCats() {

        Call<ApiObjectPublisherCats> call = global.lppcAPI.getAllPublisherCats();
        call.enqueue(new Callback<ApiObjectPublisherCats>() {


            @Override
            public void onResponse(@NonNull Call<ApiObjectPublisherCats> call, @NonNull Response<ApiObjectPublisherCats> response) {

                objectPublisherCats = response.body();

                for (int i = 0; i < objectPublisherCats.getCategorise().size(); i++) {
                    String itemName = objectPublisherCats.getCategorise().get(i).getCategory_name();
                    catsList.add(i, HtmlCompat.fromHtml(itemName, HtmlCompat.FROM_HTML_MODE_LEGACY).toString());
                }
                mCallback.loadComplete();
                initPublisherCatsRecyclerView();
            }

            @Override
            public void onFailure(@NonNull Call<ApiObjectPublisherCats> call, @NonNull Throwable t) {

            }
        });

    }

    private void initPublisherCatsRecyclerView() {
        RecyclerView publisherCats = binding.publisherComponentRecyclerview.componentRecyclerViewCats;

        CenterLayoutManager centerLayoutManager = new CenterLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        Adapter_category adapterArticleCats = new Adapter_category(catsList);
        publisherCats.setAdapter(adapterArticleCats);
        adapterArticleCats.setOnItemClickListener(new Adapter_category.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                catsSelectedPosition = position;
                centerLayoutManager.smoothScrollToPosition(publisherCats, new RecyclerView.State(), position);
                getBooks(position);
                adapterArticleCats.notifyDataSetChanged();
            }
        });
        publisherCats.setLayoutManager(centerLayoutManager);
    }

    private void getBooks(int position) {
        RecyclerView publisherContain = binding.publisherComponentRecyclerview.componentRecyclerView;

        Call<ApiObjectPublisherContain> call = global.lppcAPI.getPublisherContain(objectPublisherCats.getCategorise().get(position).getSlug());
        call.enqueue(new Callback<ApiObjectPublisherContain>() {

            @Override
            public void onResponse(@NonNull Call<ApiObjectPublisherContain> call, @NonNull Response<ApiObjectPublisherContain> response) {

                ApiObjectPublisherContain objectPublisherContain = response.body();
                Adapter_publisherContain Adapter_publisherContain = null;

                if (objectPublisherContain.getMessage() == null) {
                    publisherContain.setLayoutManager(new GridLayoutManager(getContext(), 2));
//                authorContain.addItemDecoration(new GridSpacingItemDecoration(3, 0, true));
                    Adapter_publisherContain = new Adapter_publisherContain(objectPublisherContain);
                    Adapter_publisherContain.setOnItemClickListener(new Adapter_publisherContain.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            Intent intent = new Intent(getActivity(), Activity_webView.class);
                            intent.putExtra("LinkURL", objectPublisherContain.getPosts().get(position).getPost_url());
                            startActivity(intent);
                        }
                    });
                } else {
                    Toast.makeText(getActivity(), "該分類查無資料", Toast.LENGTH_LONG).show();
                }

                publisherContain.setAdapter(Adapter_publisherContain);
            }

            @Override
            public void onFailure(Call<ApiObjectPublisherContain> call, Throwable t) {

            }
        });
    }

    @Override
    public void loadComplete() {
        getBooks(0);
    }

}