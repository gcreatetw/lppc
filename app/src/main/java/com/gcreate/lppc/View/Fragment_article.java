package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gcreate.lppc.Adapter.Adapter_articleCats;
import com.gcreate.lppc.ItemDatas.CatsModel;
import com.gcreate.lppc.ItemDatas.ItemLists;
import com.gcreate.lppc.R;
import com.gcreate.lppc.databinding.FragmentArticleBindingImpl;
import com.gcreate.lppc.global;

import java.util.List;


public class Fragment_article extends Fragment {

    private FragmentArticleBindingImpl binding;

    public Fragment_article() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article, container, false);

        initView();

        return binding.getRoot();
    }

    private void initView() {
        binding.articleComponentToolbar.componentToolbarTitle.setText("文章");
        binding.articleComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.articleComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        binding.articleComponentImage.componentBannerImage.setImageResource(R.mipmap.banner_post);

        bindRecyclerViewData();
    }

    private void bindRecyclerViewData() {

        List<CatsModel> list = ItemLists.articleCatsList();

        binding.componentRecyclerview.componentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        Adapter_articleCats articleCatsAdapter = new Adapter_articleCats(list);
        articleCatsAdapter.setOnItemClickListener(new Adapter_articleCats.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (position) {
                    case 0:
                        // 寫書人
                        global.replaceFragment(getActivity(), new Fragment_article_bookWriter(),list.get(position).getItemName_CHT());
                        break;
                    case 1:
                        // 出書人
                        global.replaceFragment(getActivity(), new Fragment_article_publisher(),list.get(position).getItemName_CHT());
                        break;
                    case 2:
                        // 說書人
                        Intent intent = new Intent(getActivity(), Activity_webView.class);
                        intent.putExtra("LinkURL", "https://www.lppc.com.tw/%e8%aa%aa%e6%9b%b8%e4%ba%ba/");
                        startActivity(intent);
                        break;
                    case 3:
                        // 最新消息
                        global.replaceFragment(getActivity(), new Fragment_article_latestNews(),list.get(position).getItemName_CHT());
                        break;
                }
            }
        });
        binding.componentRecyclerview.componentRecyclerView.setAdapter(articleCatsAdapter);
    }

}