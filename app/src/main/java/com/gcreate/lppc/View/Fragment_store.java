package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gcreate.lppc.Adapter.Adapter_storeCats;
import com.gcreate.lppc.ItemDatas.CatsModel;
import com.gcreate.lppc.ItemDatas.ItemLists;
import com.gcreate.lppc.R;
import com.gcreate.lppc.databinding.FragmentStoreBinding;

import java.util.List;


public class Fragment_store extends Fragment {

    private FragmentStoreBinding binding;

    public Fragment_store() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store, container, false);

        initView();

        return binding.getRoot();
    }

    private void initView() {
        binding.storeComponentToolbar.componentToolbarTitle.setText("商店");
        binding.storeComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.storeComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        binding.storeComponentImage.componentBannerImage.setImageResource(R.mipmap.banner_store);

        bindRecyclerViewData();
    }

    private void bindRecyclerViewData() {
        List<CatsModel> list = ItemLists.list2();

        binding.storeComponentRecyclerview.componentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        Adapter_storeCats storeCatsAdapter = new Adapter_storeCats(list);
        storeCatsAdapter.setOnItemClickListener(new Adapter_storeCats.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), Activity_webView.class);
                if (position == 0) {
                    // 雲商店
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/cloud-store/");
                } else if (position == 1) {
                    // 雲讀書會
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/%e9%9b%b2%e8%ae%80%e6%9b%b8%e6%9c%83/");
                } else if (position == 2) {
                    // 雲學苑
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/yun-xueyuan/all-yunxueyuan/");
                } else if (position == 3) {
                    // 訂閱誌
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/%e8%a8%82%e9%96%b1%e8%aa%8c/");
                }
                startActivity(intent);
            }
        });
        binding.storeComponentRecyclerview.componentRecyclerView.setAdapter(storeCatsAdapter);
    }

}