package com.gcreate.lppc.View;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gcreate.lppc.Adapter.Adapter_memberCats;
import com.gcreate.lppc.ItemDatas.ItemLists;
import com.gcreate.lppc.R;
import com.gcreate.lppc.databinding.FragmentMemberBinding;

import java.util.List;

public class Fragment_member extends Fragment {

    private FragmentMemberBinding binding;

    public Fragment_member() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_member, container, false);

        initView();

        return binding.getRoot();
    }

    private void initView() {
        binding.memberComponentToolbar.componentToolbarTitle.setText("會員");
        binding.memberComponentToolbar.componentToolbar.setNavigationIcon(R.drawable.toolbar_icon_back);
        binding.memberComponentToolbar.componentToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        bindRecyclerViewData();

    }

    private void bindRecyclerViewData() {
        List<String> list = ItemLists.memberItemList();

        binding.memberComponentRecyclerview.componentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        Adapter_memberCats memberCatsAdapter = new Adapter_memberCats(list);
        memberCatsAdapter.setOnItemClickListener(new Adapter_memberCats.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getActivity(), Activity_webView.class);
                if (position == 0) {
                    // 會員服務
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/my_account/");
                } else if (position == 1) {
                    // 聯絡我們
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/contact/");
                } else if (position == 2) {
                    // 常見問題
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/problem/");
                } else if (position == 3) {
                    // 個資&隱私權
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/privacy/");
                } else if (position == 4) {
                    // 友站連結
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/useful-links/");
                }
                startActivity(intent);
            }
        });
        binding.memberComponentRecyclerview.componentRecyclerView.setAdapter(memberCatsAdapter);
    }

}