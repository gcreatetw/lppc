package com.gcreate.lppc.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.lppc.ItemDatas.CatsModel;
import com.gcreate.lppc.R;

import java.util.List;

public class Adapter_homeCats_2 extends RecyclerView.Adapter<Adapter_homeCats_2.ViewHolder> {

    // 首頁大分類， 雲商店、雲讀書會、雲學苑、訂閱誌。
    private final List<CatsModel> catsModelsList;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_homeCats_2(List<CatsModel> catsModelsList) {
        this.catsModelsList = catsModelsList;
    }
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_style2, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.itemImage.setBackgroundResource(catsModelsList.get(position).getImage());
        holder.itemName_CHT.setText(catsModelsList.get(position).getItemName_CHT());
        holder.itemName_EN.setText(catsModelsList.get(position).getItemName_EN());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(holder.itemView, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (catsModelsList == null) ? 0 : catsModelsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView itemImage;
        private TextView itemName_CHT;
        private TextView itemName_EN;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.itemImage = itemView.findViewById(R.id.itemImage);
            this.itemName_CHT = itemView.findViewById(R.id.item_name_cht);
            this.itemName_EN = itemView.findViewById(R.id.item_name_en);
        }
    }
}
