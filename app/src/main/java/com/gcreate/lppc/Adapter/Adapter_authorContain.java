package com.gcreate.lppc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gcreate.lppc.R;
import com.gcreate.lppc.WebAPI.ApiObjectBookWriterContain;

import java.util.List;

public class Adapter_authorContain extends RecyclerView.Adapter<Adapter_authorContain.ViewHolder> {

    // 標題分類內容
    private Context mContext;
    private ApiObjectBookWriterContain objectBookWriterContain;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_authorContain(ApiObjectBookWriterContain objectBookWriterContain) {
        this.objectBookWriterContain = objectBookWriterContain;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_author, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        List<ApiObjectBookWriterContain.RelatedAuthorsBean> bean = objectBookWriterContain.getRelated_authors();

        holder.tv_itemTitle.setText(bean.get(position).getAuthor_name());

        Glide.with(mContext).load(bean.get(position).getAuthor_picture_url()).centerCrop()
                .error(R.drawable.icon_avatar)
                .placeholder(R.drawable.circle_graph)
                .transform(new GlideCircleTransform(mContext, 2, mContext.getResources().getColor(R.color.gray_AFAFAF)))
                .into(holder.img_author);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(holder.itemView, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return objectBookWriterContain == null ? 0 : objectBookWriterContain.getRelated_authors().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_itemTitle;
        private ImageView img_author;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_itemTitle = itemView.findViewById(R.id.tv_titleHealth);
            this.img_author = itemView.findViewById(R.id.img_author);
        }
    }
}
