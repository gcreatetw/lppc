package com.gcreate.lppc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.gcreate.lppc.R;
import com.gcreate.lppc.WebAPI.ApiObjectPublisherContain;

import java.util.List;

public class Adapter_publisherContain extends RecyclerView.Adapter<Adapter_publisherContain.ViewHolder> {

    // 出書人內容
    private Context mContext;
    private ApiObjectPublisherContain objectPublisherContain;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_publisherContain(ApiObjectPublisherContain objectPublisherContain) {
        this.objectPublisherContain = objectPublisherContain;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_publisher, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        List<ApiObjectPublisherContain.PostsBean> bean = objectPublisherContain.getPosts();

        Glide.with(mContext).load(bean.get(position).getPost_thumbnail_url()).apply(RequestOptions.centerCropTransform()).into(holder.img_publisher);

        holder.tv_date.setText(bean.get(position).getPost_date());
        holder.tv_bookTitle.setText(bean.get(position).getPost_title());
        holder.tv_bookSubTitle.setText(HtmlCompat.fromHtml(bean.get(position).getPost_abstract(), HtmlCompat.FROM_HTML_MODE_LEGACY).toString());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(holder.itemView, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return objectPublisherContain == null ? 0 : objectPublisherContain.getPosts().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_publisher;
        private TextView tv_date, tv_bookTitle, tv_bookSubTitle;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.img_publisher = itemView.findViewById(R.id.img_publisher);
            this.tv_date = itemView.findViewById(R.id.tv_date);
            this.tv_bookTitle = itemView.findViewById(R.id.tv_bookTitle);
            this.tv_bookSubTitle = itemView.findViewById(R.id.tv_bookSubTitle);

        }
    }
}
