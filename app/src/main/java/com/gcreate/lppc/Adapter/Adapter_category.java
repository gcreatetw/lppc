package com.gcreate.lppc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.lppc.R;

import java.util.List;

public class Adapter_category extends RecyclerView.Adapter<Adapter_category.ViewHolder> {

    // 標題分類
    private Context mContext;
    private List<String> catsItem;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_category(List<String> catsItem) {
        this.catsItem = catsItem;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_categoryitem, parent, false);
        mContext = parent.getContext();
        return new ViewHolder(v);
    }

    private int selectedPosition;

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        if (position == selectedPosition) {
            holder.tv_itemTitle.setTextColor(ContextCompat.getColor(mContext, R.color.red_E60012));
            holder.img_itemUnderline.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_E60012));
        } else {
            holder.tv_itemTitle.setTextColor(ContextCompat.getColor(mContext, R.color.gray_666666));
            holder.img_itemUnderline.setBackgroundColor(ContextCompat.getColor(mContext, R.color.white));
        }

        holder.tv_itemTitle.setText(catsItem.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedPosition = position;
                onItemClickListener.onItemClick(holder.itemView, position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return catsItem.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_itemTitle;
        private View img_itemUnderline;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tv_itemTitle = itemView.findViewById(R.id.tv_title);
            this.img_itemUnderline = itemView.findViewById(R.id.img_underline);
        }
    }
}
