package com.gcreate.lppc.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gcreate.lppc.ItemDatas.CatsModel;
import com.gcreate.lppc.R;

import java.util.List;

public class Adapter_memberCats extends RecyclerView.Adapter<Adapter_memberCats.ViewHolder> {

    // 會員大分類， 會員服務、聯絡我們、常見問題、個資&隱私權、友站連結。
    private final List<String> itemList;
    private OnItemClickListener onItemClickListener = null;

    public Adapter_memberCats(List<String> itemList) {
        this.itemList = itemList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        onItemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_memberitem, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.itemName.setText(itemList.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClick(holder.itemView, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (itemList == null) ? 0 : itemList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView itemName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.itemName = itemView.findViewById(R.id.tv_title);

        }
    }
}
