package com.gcreate.lppc.ItemDatas;

public class CatsModel {

    private int image;
    private String itemName_CHT;
    private String itemName_EN;

    public CatsModel(int image, String itemName_CHT, String itemName_EN) {
        this.image = image;
        this.itemName_CHT = itemName_CHT;
        this.itemName_EN = itemName_EN;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getItemName_CHT() {
        return itemName_CHT;
    }

    public void setItemName_CHT(String itemName_CHT) {
        this.itemName_CHT = itemName_CHT;
    }

    public String getItemName_EN() {
        return itemName_EN;
    }

    public void setItemName_EN(String itemName_EN) {
        this.itemName_EN = itemName_EN;
    }

}
