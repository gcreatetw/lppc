package com.gcreate.lppc.ItemDatas;

import com.gcreate.lppc.R;

import java.util.ArrayList;
import java.util.List;

public class ItemLists {

    public static List<CatsModel> list1() {

        List<CatsModel> catsModelList_1 = new ArrayList<>();
        catsModelList_1.add(0, new CatsModel(R.mipmap.image_home_book_writer, "寫書人", "BOOK WRITER"));
        catsModelList_1.add(1, new CatsModel(R.mipmap.image_home_publisher, "出書人", "PUBLISHER"));
        catsModelList_1.add(2, new CatsModel(R.mipmap.image_home_storyteller, "說書人", "STORYTELLER"));
        catsModelList_1.add(3, new CatsModel(R.mipmap.image_home_latest_news, "最新消息", "LATEST NEWS"));

        return catsModelList_1;
    }

    public static List<CatsModel> list2() {
        List<CatsModel> catsModelList_2 = new ArrayList<>();
        catsModelList_2.add(0, new CatsModel(R.mipmap.image_cloud_store, "雲商店", "BOOK CLOUD STORE"));
        catsModelList_2.add(1, new CatsModel(R.mipmap.image_cloud_book_club, "雲讀書會", "CLOUD BOOK CLUB"));
        catsModelList_2.add(2, new CatsModel(R.mipmap.image_cloud_academy, "雲學苑", "CLOUD ACADEMY"));
        catsModelList_2.add(3, new CatsModel(R.mipmap.image_subscription_monthly, "訂閱誌", "SUBSCRIPTION"));

        return catsModelList_2;
    }

    public static List<CatsModel> articleCatsList() {
        List<CatsModel> articleList = new ArrayList<>();
        articleList.add(0, new CatsModel(R.mipmap.image_article_book_writer, "寫書人", "BOOK WRITER"));
        articleList.add(1, new CatsModel(R.mipmap.image_article_publisher, "出書人", "PUBLISHER"));
        articleList.add(2, new CatsModel(R.mipmap.image_article_storyteller, "說書人", "STORYTELLER"));
        articleList.add(3, new CatsModel(R.mipmap.image_article_latest_news, "最新消息", "LATEST NEWS"));

        return articleList;
    }

    public static List<String> memberItemList() {
        List<String> itemList = new ArrayList<>();
        itemList.add(0, "會員服務");
        itemList.add(1, "聯絡我們");
        itemList.add(2, "常見問題");
        itemList.add(3, "個資&隱私權");
        itemList.add(4, "友站連結");

        return itemList;
    }
}
