package com.gcreate.lppc.WebAPI;

import java.util.List;

public class ApiObjectBookWriterContain {

    private List<RelatedAuthorsBean> related_authors;

    public List<RelatedAuthorsBean> getRelated_authors() {
        return related_authors;
    }

    public void setRelated_authors(List<RelatedAuthorsBean> related_authors) {
        this.related_authors = related_authors;
    }

    public static class RelatedAuthorsBean {
        /**
         * author_name : 張本聖
         * author_url : https://www.lppc.com.tw/author/ben306?term_id=289
         * author_picture_url : https://www.lppc.com.tw/wp-content/uploads/bs.jpg
         */

        private String author_name;
        private String author_url;
        private String author_picture_url;

        public String getAuthor_name() {
            return author_name;
        }

        public void setAuthor_name(String author_name) {
            this.author_name = author_name;
        }

        public String getAuthor_url() {
            return author_url;
        }

        public void setAuthor_url(String author_url) {
            this.author_url = author_url;
        }

        public String getAuthor_picture_url() {
            return author_picture_url;
        }

        public void setAuthor_picture_url(String author_picture_url) {
            this.author_picture_url = author_picture_url;
        }
    }
}
