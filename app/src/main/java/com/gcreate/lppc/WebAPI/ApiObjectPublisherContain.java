package com.gcreate.lppc.WebAPI;

import java.util.List;

public class ApiObjectPublisherContain {

    private List<PostsBean> posts;

    public List<PostsBean> getPosts() {
        return posts;
    }

    public static class PostsBean {
        /**
         * post_title : 關鍵人物
         * post_date : 2021-02-03
         * post_abstract : 一家三代綑綁在一張無形的關係網中，老爸的心願、兒子的鬱結，全通過媳婦一人維繫。雖互相牽制，卻也有改變的可能。
         * post_thumbnail_url : https://www.lppc.com.tw/wp-content/uploads/09wylee-e1608690794446.jpg
         * post_url  : "https://www.lppc.com.tw/publisher/%e9%97%9c%e9%8d%b5%e4%ba%ba%e7%89%a9"
         */

        private String post_title;
        private String post_date;
        private String post_abstract;
        private String post_thumbnail_url;
        private String post_url;

        public String getPost_title() {
            return post_title;
        }

        public String getPost_url() {
            return post_url;
        }

        public String getPost_date() {
            return post_date;
        }

        public String getPost_abstract() {
            return post_abstract;
        }

        public String getPost_thumbnail_url() {
            return post_thumbnail_url;
        }

    }

    /**
     * message : No category post found.
     */
    private String message;

    public String getMessage() {
        return message;
    }

}
