package com.gcreate.lppc.WebAPI;

import java.util.List;

public class ApiObjectPublisherCats {

    private List<CategoriseBean> categorise;

    public List<CategoriseBean> getCategorise() {
        return categorise;
    }

    public void setCategorise(List<CategoriseBean> categorise) {
        this.categorise = categorise;
    }

    public static class CategoriseBean {
        /**
         * category_name : 全部
         * slug : all-publishers
         */

        private String category_name;
        private String slug;

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getSlug() {
            return slug;
        }

        public void setSlug(String slug) {
            this.slug = slug;
        }
    }
}
