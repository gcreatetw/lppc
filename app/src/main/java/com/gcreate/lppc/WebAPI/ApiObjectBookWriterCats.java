package com.gcreate.lppc.WebAPI;

import java.util.List;

public class ApiObjectBookWriterCats {


    private List<CategoriseBean> categorise;

    public List<CategoriseBean> getCategorise() {
        return categorise;
    }


    public static class CategoriseBean {
        /**
         * category_name : 全部
         * slug : all-writers
         */

        private String category_name;
        private String slug;

        public String getCategory_name() {
            return category_name;
        }


        public String getSlug() {
            return slug;
        }


    }
}
