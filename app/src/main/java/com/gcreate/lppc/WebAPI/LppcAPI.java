package com.gcreate.lppc.WebAPI;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface LppcAPI {

    String PATH = "www.lppc.com.tw/";
    String SERVER_IP = "https://" + PATH + "wp-json/";

    //------------------     GET ----------------------------
    // 取得寫書人作者分類
    @Headers({"Content-Type: application/json"})
    @GET("get_book_writer_categorise_api/v1?cats")
    Call<ApiObjectBookWriterCats> getAllBookWriterCats();

    // 取得寫書人作者分類內容
    @Headers({"Content-Type: application/json"})
    @GET("get_book_writer_categorise_api/v1?")
    Call<ApiObjectBookWriterContain> getBookWriterContain(@Query("cat") String cats);

    // 取得出書人作者分類
    @Headers({"Content-Type: application/json"})
    @GET("get_publisher_categorise_api/v1?cats")
    Call<ApiObjectPublisherCats> getAllPublisherCats();

    // 取得寫書人作者分類內容
    @Headers({"Content-Type: application/json"})
    @GET("get_publisher_categorise_api/v1?")
    Call<ApiObjectPublisherContain> getPublisherContain(@Query("cat") String cats);

    // 取得最新消息
    @Headers({"Content-Type: application/json"})
    @GET("get_latest_news_api/v1?get")
    Call<ApiObjectPublisherContain> getLatestNews();

}
