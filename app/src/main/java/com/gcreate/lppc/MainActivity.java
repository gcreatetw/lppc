package com.gcreate.lppc;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.gcreate.lppc.View.Activity_webView;
import com.gcreate.lppc.View.Fragment_article;
import com.gcreate.lppc.View.Fragment_home;
import com.gcreate.lppc.View.Fragment_member;
import com.gcreate.lppc.View.Fragment_store;
import com.gcreate.lppc.databinding.ActivityMainBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private int selectedPosition = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initView();

        binding.bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.item_home) {
                    selectedPosition = 0;
                    replaceFragment(new Fragment_home());
                } else if (id == R.id.item_article) {
                    selectedPosition = 1;
                    replaceFragment(new Fragment_article());
                } else if (id == R.id.item_store) {
                    selectedPosition = 2;
                    replaceFragment(new Fragment_store());
                } else if (id == R.id.item_cart) {
                    selectedPosition = 3;
                    Intent intent = new Intent(MainActivity.this, Activity_webView.class);
                    intent.putExtra("LinkURL", "https://www.lppc.com.tw/cart/");
                    startActivity(intent);
                } else if (id == R.id.item_member) {
                    selectedPosition = 4;
                    replaceFragment(new Fragment_member());
                }

                return true;
            }
        });

    }

    private void initView() {
        replaceFragment(new Fragment_home());
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment, "");
        fragmentTransaction.addToBackStack(fragment.toString());
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStackImmediate();
            if (fragmentManager.getFragments().get(0).toString().startsWith("Fragment_home")) {
                binding.bottomNavigationView.getMenu().findItem(R.id.item_home).setChecked(true);
            } else if (fragmentManager.getFragments().get(0).toString().startsWith("Fragment_article")) {
                binding.bottomNavigationView.getMenu().findItem(R.id.item_article).setChecked(true);
            } else if (fragmentManager.getFragments().get(0).toString().startsWith("Fragment_store")) {
                binding.bottomNavigationView.getMenu().findItem(R.id.item_store).setChecked(true);
            } else if (fragmentManager.getFragments().get(0).toString().startsWith("Fragment_member")) {
                binding.bottomNavigationView.getMenu().findItem(R.id.item_member).setChecked(true);
            }

        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (selectedPosition == 0 || selectedPosition == 3) {
            binding.bottomNavigationView.getMenu().findItem(R.id.item_home).setChecked(true);
        } else if (selectedPosition == 4) {
            binding.bottomNavigationView.getMenu().findItem(R.id.item_member).setChecked(true);
        }
    }
}